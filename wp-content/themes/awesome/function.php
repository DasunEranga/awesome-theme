<?php

/** calling for theme stylesheets **/
function e25b_style()
{
    wp_enqueue_style('poc-custom-css', get_template_directory_uri() . '/assets/css/style.css', array());
}
add_action('wp_enqueue_scripts', 'e25b_style');

/** include custom jQuery **/
function e25b_scripts()
{
    /* class jquery using CDN */
    wp_enqueue_script('jquery-js', get_template_directory_uri() . '/assets/js/jquery.min.js', array());
    /* class jquery using local file */
    wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array());
    wp_enqueue_script('slick-js', get_template_directory_uri() . '/assets/js/slick.min.js', array());
    wp_enqueue_script('equal-height-js', get_template_directory_uri() . '/assets/js/match-height.js', array());
}
add_action('wp_enqueue_scripts', 'e25b_scripts');

/** include custom jQuery to footer **/
function e25b_scripts_footer()
{
    wp_enqueue_script('custom-js', get_template_directory_uri() . '/assets/js/custom.js', array());
}
add_action('wp_footer', 'e25b_scripts_footer');
